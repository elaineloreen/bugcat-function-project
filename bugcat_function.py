'''
The working principles of a bugcat/function.

functions are declared using the following syntax:
'''

def bugcat_eats(some_food):
    return some_food + " converted into poop"

'''
The def keyword indicates that you are defining a function.

bugcat_eats is the name of our function.
It can be named anything you like, however, being descriptive is preferred.

Within the parentheses are our arguments or parameters (words typically used interchangeably).
These are variables that you pass into the function.
There can be zero or more parameters to a function.
If you wish to pass in more parameters, it would be in the form of a comma separated list.
For example: (some_food, some_more_food)

Finally, we see the return value.
This is some data that we wish to send as an output from our function.
We usually store the returned value in a variable to do some work using it.

Below is an example of how a function is called/executed.
'''

bugcat_output = bugcat_eats("burgers")

print("My bugcat has produced: " + bugcat_output)

'''
Your task:

1. Create a function named bugcat_plays
2. This function should take in 2 parameters named toy and duration respectively
3. This function should print a message in the format "Bugcat plays with {toy} for {duration} minutes"
4. It should also return the toy variable as is.
5. Finally, execute/call your bugcat_plays function with the following sets of parameters:
    - ("teaser", 30)
    - ("ball", 10)
    - ("human shoe", 50)
    
Example execution code:
returned_toy = bugcat_plays("teaser", 30)
print("Bugcat returned toy " + returned_toy); 

Example output:
>> Bugcat plays with teaser for 30 minutes
>> Bugcat returned toy teaser


++++++++++++++++++++
Super Bonus Question:
++++++++++++++++++++
Time to do some research into random number generation:
https://pythonbasics.org/random-numbers/

Modify your existing bugcat_plays function with the following behaviour:
- generate a random number between 0 and 1
- if the number is greater than 0.5, then the behaviour of the function would be the same as before
- if the number if less than 0.5, the bugcat has destroyed the toy. This has the following consequences:
    - There should be an extra message printed in the format "Oh no! {toy} was destroyed!"
    - The return value of the function should be "destroyed {toy}" instead of just the toy as is.
    
'''

# Enter your code here